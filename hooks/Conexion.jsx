const URL = "https://fakerestapi.azurewebsites.net";
/*
export const InicioSesion = async (data) => {
    const headers = {
        'Accept': 'application/json',
        "Content-Type": "application/json"
    };
    const datos = await (await fetch(URL + "/sesion", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    //console.log(datos);
    return datos;
}
*/


//traer libros
export const Libro = async () => {
    const datos = await (await fetch(URL + "/api/v1/Books", {
        method: "GET"
        
    })).json();
    
    return datos;
}

//traer libros por id
export const ListarporID = async (key) => {
    const cabeceras = {        
        "X-API-KEY": key
    };
    const datos = await (await fetch(URL + `/api/v1/Books/${key}`, {
        method: "GET",
        headers: cabeceras
    })).json();    
    return datos;
}
//Guardar Libros
export const GuardarLibros = async (data, key) => {
    const headers = {
        'Accept': 'application/json',
        "X-API-KEY": key
    };
    const datos = await (await fetch(URL + "/api/v1/Books", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    //console.log(datos);
    return datos;
}
//actualizar
export const ActualizarLibros = async (data) => {
    const headers = {
        'Accept': 'application/json',
        'Content-Type':'application/json'
       
    };
    const datos = await (await fetch(URL + `/api/v1/Books/${data}`, {
        method: "PUT",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    //console.log(datos);
    return datos;
}


// 



