
import './App.css';
import { Routes, Route } from 'react-router-dom';
import Principal from './fragment/Principal';
import ListaLibro from './fragment/ListarLibros';
import RegistroLibro from './fragment/RegistroLibro';
import ModificarLibro from './fragment/ModificarLibro';
import ListarLibroID from './fragment/ListarLibroID';

function App() {
  return (
    <div className="App">

      <Routes>
      <Route path='/' element={<Principal/>} exact/>
      <Route path='/libros' element={<ListaLibro/>}/>
      <Route path='/registro' element={<RegistroLibro/>}/>
      <Route path='/modificar' element={<ModificarLibro/>}/>
      <Route path='/listarID' element={<ListarLibroID/>}/>
      </Routes>
    </div>
  );
}

export default App;
