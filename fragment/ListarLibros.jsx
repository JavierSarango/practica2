import '../css/stylea.css';
import '../css/style.css';

//import 'bootstrap/dist/css/bootstrap.min.css';

import Table from 'react-bootstrap/Table';
import Header from "./Header";
import Footer from './Footer';
import { Libro } from '../hooks/Conexion';
import { useEffect, useState } from 'react';
const ListaLibro = () => {

    const [data, setData] = useState([]);

  

    Libro().then((info) => {
        //console.log(info);
        if (info.error === false) {
            setData(info.data);
            console.log('data: ', info.data)
        }
    });



    useEffect(() => {
        const getData = async () => {

            try {
                const response = await Libro();
                console.log(response.data);
                setData(response);
            } catch (error) {
                console.log("hola");
                console.error(error);
            }
        };
        getData();

    }, []);



    return (

        <div className="wrapper" >
            <div className="d-flex flex-column">
                <div className="content">
                    <Header />
                    {/** DE AQUI CUERPO */}

                    <div className='container-fluid'>
                        <a className='btn btn-success' href={'/registro'} style={{ marginLeft: '1180px', marginBottom: '20px' }}>Agregar Libro</a>
                        <div>
                            <Table striped>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Título</th>
                                        <th>Descripción</th>
                                        <th>Número de Páginas</th>
                                        <th>Estracto</th>
                                        <th>Fecha de Publicación</th>
                                        <th>Operaciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {data.map(item => (
                                        <tr key={item.id}>
                                            <td>{item.id}</td>
                                            <td>{item.title}</td>
                                            <td>{item.description}</td>
                                            <td>{item.pageCount}</td>
                                            <td>{item.excerpt}</td>
                                            <td>{item.publishDate}</td>
                                            <td>

                                                <a className='btn btn-warning' href="/modificar">Modificar</a>
                                                <a className='btn btn-danger' href={'/registro'} style={{ marginTop: '10px' }}>Eliminar</a>
                                            </td>
                                        </tr>
                                    ))}

                                </tbody>
                            </Table>

                            
                        </div>

                    </div>
                </div>
            </div>
            <Footer />
        </div>


    );
}

export default ListaLibro;