
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import './../App.css';
const Principal = () => {
  return (
    <div className="App">

      <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Brand href="/">Biblioteca</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link href="/libros">Listar Libros</Nav.Link>
              <Nav.Link href="/registro">Agregar Libros</Nav.Link>
              <Nav.Link href="/listarID">Buscar Libros</Nav.Link>
              
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <div id='Fondo'>
        <img className="imagen-libreria"
          src={require('../imagenes/libreria.jpg')}
          alt='imagen de fondo' />
      </div>


    </div>
  );
}

export default Principal;