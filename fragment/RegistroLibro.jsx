
import '../css/stylea.css';
import '../css/style.css';
import Header from "./Header";
import Footer from './Footer';
import { useNavigate } from 'react-router-dom';

import { GuardarLibros } from '../hooks/Conexion';
import mensajes from '../utilidades/Mensajes';
import { useForm } from 'react-hook-form';
const RegistroLibro = () => {
    const navegation = useNavigate();
    const { register, handleSubmit, formState: { errors } } = useForm();

    //acciones
    //submit
    const onSubmit = (data) => {

        var datos = {
            "title": data.titulo,
            "description": data.descripcion,
            "pageCount": data.numpag,
            "excerpt": data.estracto,
            "publishDate": data.publicacion,

        };
        GuardarLibros(datos).then((info) => {
            //console.log(info);
            if (info.error === true) {
                mensajes(info.message, 'error', 'Error')
            } else {
                mensajes(info.message);
                navegation('/libros');
            }
        }
        );
    };


    return (
        <div className="wrapper">
            <div className="d-flex flex-column ">
                <div className="content">
                    <Header />
                    {/** DE AQUI CUERPO */}

                    <div className='container-fluid' >
                        <div className="col-lg-10">
                            <div className="p-5">
                                <div id='contenido' className="text-center">
                                    <h1 className="h4 text-gray-900 mb-4">Registro de Libros!</h1>
                                </div>
                                <form className="user" onSubmit={handleSubmit(onSubmit)}>
                                    <div className="form-group">
                                        <input type="text" {...register('titulo', { required: true })} className="form-control form-control-user" placeholder="Ingrese el Título" />
                                        {errors.titulo && errors.titulo.type === 'required' && <div className='alert alert-danger'>Ingrese un titulo</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese la descripcion" {...register('descripcion', { required: true })} />
                                        {errors.descripcion && errors.descripcion.type === 'required' && <div className='alert alert-danger'>Ingrese una descripcion</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="number" className="form-control form-control-user" placeholder="Ingrese el Número de páginas" {...register('numpag', { required: true })} />
                                        {errors.numpag && errors.numpag.type === 'required' && <div className='alert alert-danger'>Ingrese un número de página válido</div>}
                                    </div>

                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el estracto" {...register('estracto', { required: true })} />
                                        {errors.estracto && errors.estracto.type === 'required' && <div className='alert alert-danger'>Ingrese un estracto</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="date" className="form-control form-control-user" placeholder="Ingrese la fecha de publicación" {...register('publicacion', { required: true })} />
                                        {errors.publicacion && errors.publicacion.type === 'required' && <div className='alert alert-danger'>Ingrese una fecha de publicacion</div>}
                                    </div>
              
                                    <hr />
                                    <a href={"/libros"} className="btn btn-google btn-user btn-block">
                                        <i className="fab fa-google fa-fw"></i> Cancelar
                                    </a>
                                    <input className="btn btn-facebook btn-user btn-block" type='submit' value="REGISTRAR"></input>

                                </form>
                                <hr />

                                <div className="text-center">
                                    <a className="small" href="register.html">Create an Account!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}

export default RegistroLibro;