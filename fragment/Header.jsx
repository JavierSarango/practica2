import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

const Header = () => {
    //const cargarImagen = require.context("../img");
    return (
        <nav className="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

            <Navbar bg="light" expand="lg">
                <Container>
                    <Navbar.Brand href="/">Biblioteca</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link href="/libros">Listar Libros</Nav.Link>
                            <Nav.Link href="/registro">Agregar Libros</Nav.Link>
                            <Nav.Link href="/listarID">Buscar Libros</Nav.Link>
                            
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>


        </nav>
    );
}

export default Header;